"use strict";

import {Color} from "/chess/color.js";

/**
 * Field of a chessboard.
 */

export default class Tile {
    
    /**
     * 
     * @param {number} posX - x square on the canvas
     * @param {number} posY - y square on the canvas
     * @param {number} size - size of the drawn rectangle in px
     * @param {string} color - color of the Tile 
     * @param {string} square - square on the chessboard (a1 to h8)
     * @param {Game} game - Game object with which this Tile is associated.
     * Meaning it is part of the chessboard which is again part of the Game.
     */

    constructor(posX, posY, size, color, square, game){
        this.posX = posX;
        this.posY = posY;
        this.color = color;
        this.size = size;
        this.square = square;
        this.chessPiece = null;
        this.game = game;
    }

    /**
     * Place the ChessPiece on this Tile 
     * and render it to make it visible.
     * 
     * @param {ChessPiece} chessPiece 
     * @returns {void}
     */

    placeChessPiece(chessPiece){
        if (!chessPiece) return;
        
        this.chessPiece = chessPiece;
        let offset = parseInt(this.size / 8);
        this.chessPiece.posX = this.posX + offset;
        this.chessPiece.posY = this.posY + offset;
        this.chessPiece.tile = this;

        this.render();
    }

    /**
     *  Checks if the this Tile has a ChessPiece placed on top of it.
     * @returns {boolean}
     */

    hasChessPiece(){
        return (this.chessPiece != null);
    }

    /**
     * Highlights this Tile so it is dyed greenish.
     * @returns {void}
     */

    highlight(){
        if (this.game.chessboard.currentlyHightlighted.has(this)) return;
        let ctx = this.game.ctx;
        
        // Reduce opacity to ensure that ChessPieces 
        // that are placed on top of it are still visible.
        ctx.globalAlpha = 0.4;
        ctx.fillStyle = "green";
        
        ctx.fillRect(this.posX, this.posY, this.size, this.size);

        // After drawing the green rectangle revert the opacity back 
        // to the standard of 1.
        ctx.globalAlpha = 1;
    }

    /**
     * Renders/Draws this Tile on the canvas. If it has a square display
     * or a ChessPiece it is also rendered.
     * @returns {void}
     */

    render(){
        let ctx = this.game.ctx;
        ctx.clearRect(this.posX, this.posY, this.size, this.size);
        ctx.fillStyle = this.color;
        ctx.fillRect(this.posX, this.posY, this.size, this.size);
        let offsetPosX = this.size * 0.05,
            offsetPosY = this.size * 0.2;
        
        let squareDisplayWhite = (this.game.player.color === Color.WHITE &&
            (this.square[0] === "a" || this.square[1] === "1"));
        
        let squareDisplayBlack = (this.game.player.color === Color.BLACK && 
            (this.square[0] === "h" || (this.square[1] === "8")));
        
        if (squareDisplayWhite || squareDisplayBlack){
            ctx.font = "22px serif";
            ctx.fillStyle = "black";
            ctx.fillText(this.square, this.posX + offsetPosX, this.posY + offsetPosY);
        } 
        
        if (this.chessPiece != null) this.chessPiece.render();
    }

}