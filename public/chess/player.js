"use strict";

import Game from "/chess/game.js";
import { Color } from "/chess/color.js";

/**
 * Client-Side Player object who interacts with 
 * the chessboard and has the ability to show all legal moves,
 * choose one and make it.
 */

export default class Player {
  /**
   *  Initializes the player object with the color and the
   *  corresponding game the player is automaticially participating in.
   *  The parameter canvas is the html canvas object on which the
   *  game is taking place.
   *
   * @constructor
   * @param {Color} color - Color of the Player (White/ Black)
   * @param {Object} canvas - The canvas on which the game the player
   * participates in takes place. It gets passed to the game object as a parameter.
   * @param {Object} socket - Associated network socket.
   * @param {Object} display - Display of game for the end of the game, and 
   * which side it is to move. Similiar to the canvas it gets pass to the 
   * game constructor as a parameter.
   */

  constructor(color, canvas, socket, display) {
    this.color = color;
    this.selectedPiece = null;
    this.socket = socket;
    this.game = new Game(canvas, this, display);
  }

  /**
   * This method gets called when the player clicks on the canvas.
   * If the player clicks a Tile with a ChessPiece of his Color,
   * all valid moves are shown/ highlighted. Then the player 
   * has the option to click a highlighted Tile to move his ChessPiece
   * to that square.
   * 
   * @returns {void}
   */

  makeMove() {
    if (this.color != this.game.sideToMove) return;

    // Get the x and y postion of the mouse click.
    let rect = this.game.canvas.getBoundingClientRect(),
      mousePosX = Math.floor(event.clientX - rect.left),
      mousePosY = Math.floor(event.clientY - rect.top),
      currentTile = null;

    for (let square of Object.keys(this.game.chessboard.board)) {
      currentTile = this.game.chessboard.board[square];

      // if this condition is met the currentTile is the Tile which
      // the user has clicked.
      if (currentTile.posX <= mousePosX &&
            currentTile.posY <= mousePosY &&
            currentTile.posX + currentTile.size >= mousePosX &&
            currentTile.posY + currentTile.size >= mousePosY)
        break;
    }

    // If the tile is already highlighted move the selectedPiece there.
    // Otherwise if the tile is not already highlighted and has a ChessPiece.
    // highlight it. If neither is true call unhighlightTiles and set
    // the selectedPiece to null because the Player has clicked somewhere invalid.
    let opponentMove = false;
    if (this.game.chessboard.currentlyHightlighted.has(currentTile.square)) {
      this.selectedPiece.move(currentTile.square, opponentMove);
      this.game.chessboard.unhighlightTiles();
      this.selectedPiece = null;
      this.game.nextTurn();
    } else if (currentTile.hasChessPiece() &&
        currentTile.chessPiece.color == this.color) {
      this.game.chessboard.unhighlightTiles();
      this.selectedPiece = currentTile.chessPiece;
      this.selectedPiece.getMoves();
    } else {
      this.game.chessboard.unhighlightTiles();
      this.selectedPiece = null;
    }
  }
}
