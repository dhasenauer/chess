"use strict";
/**
 * Enum for the Color. 
 * @readonly
 * @enum {String}
 */
export const Color = {
    BLACK : "Black",
    WHITE : "White"
};