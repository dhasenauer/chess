"use strict";

/**
 * Enum for the type of a ChessPiece. 
 * @readonly
 * @enum {String}
 */

export const PieceType = {
    ROOK : "Rook",
    KNIGHT : "Knight",
    KING : "King",
    QUEEN : "Queen",
    PAWN : "Pawn",
    BISHOP : "Bishop"
};