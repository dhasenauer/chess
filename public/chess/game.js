"use strict";

import Chessboard from "/chess/chessboard.js";
import { Color } from "/chess/color.js";

/**
 * Game Class which includes one player and the
 * chessboard with its Tiles and ChessPieces.
 */

export default class Game {
  /**
   * @constructor
   * @param {Object} canvas - html canvas object on which the game is taking place
   * @param {Player} player - the player on the client side which is participating in the game
   */

  constructor(canvas, player, display) {
    this.turn = 0;
    this.sideToMove = Color.WHITE;
    this.display = display;
    this.canvas = canvas;
    this.ctx = canvas.getContext("2d");
    this.player = player;
    this.gameEnded = false;
    this.chessboard = new Chessboard(this);
  }

  /**
   * This method initiates the next turn.
   * For that it changes the side to move and updates the display.
   * 
   * @return {void}
   */

  nextTurn() {
    this.sideToMove =
        this.sideToMove == Color.WHITE ? Color.BLACK : Color.WHITE;
    this.turn++;

    // Updates the display for which side has to move.
    if (!this.gameEnded) this.display.innerHTML = `${this.sideToMove} to Move`;
  }

  /**
   * Ends the game and displays the outcome.
   * 
   * @param {String} response - Response Text from the 
   * server if the game is over
   */

  gameOver(response) {
    if (response["reason"] === "Checkmate") {
      this.display.innerHTML = `${response["winner"]} has won`;
    } else {
      this.display.innerHTML = response["reason"];
    }
    this.gameEnded = true;
  }

  /**
   * Translates Standard Algebraic Notation to square 
   * positions on the chessboard. (e.g N5f3 to f3, Bxf7+ to f7)
   * 
   * @param {String[]} moves - List of squares
   */

  translateSAN(moves, chessPiece) {
    let result = [];
    // Check if the moves includes a castling move.
    // Denoted: "O-O" for kingside castling and
    // "O-O-O" for queenside castling.
    let includesQueenSideCastlingMove = moves.includes("O-O-O"),
        includesKingSideCastlingMove = moves.includes("O-O");

    // If that is the case add to the moves the tile the ChessPiece can 
    // move to.
    if (includesQueenSideCastlingMove){
        if (chessPiece.color == Color.WHITE){
            result.push("c1");
        } else {
            result.push("c8");
          }
    } else if (includesKingSideCastlingMove){
        if (chessPiece.color == Color.WHITE){
            result.push("g1");
        } else {
          result.push("g8");
        }
      }
    
    for (let move of moves) {
      if (move.length != 2) {
        // Go throught every character of a move.
        // If a character and a number is next to each other
        // it is the move format we are looking for.
        for (let i = 0; i < move.length - 1; i++) {
          if (move[i].match(/[aA-hH]/) && 
              move[i + 1].match(/[1-8]/)) {
                move = move[i] + move[i + 1];
            }
        }
      }
      result.push(move);
    }
    return result;
  }
}
