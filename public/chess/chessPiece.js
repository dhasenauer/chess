"use strict";

import {PieceType} from "./pieceType.js";
import {Color} from "./color.js";


/**
 * "Abstract" Superclass ChessPiece which
 * consists of commonalities which all chess pieces 
 * (e.g. Queen, Rook, Pawn etc.) share. 
 * (e.g. render method for drawing the ChessPiece, move method
 * to move the ChessPiece, getMoves method for getting all available from
 * the server, and attributes like x and y position or color)
 */

export class ChessPiece { 
    
    /**
     * @constructor
     * @param {PieceType} pieceType 
     * - ChessPiece type (e.g. Rook, King, Queen etc.) 
     * @param {Tile} tile - Tile on which the ChessPiece is placed
     * @param {Color} color - Color (Enum) e.g. White/Black
     * @param {Game} game  - Associated to this game
     */

    constructor(pieceType, tile, color, game) {
        this.pieceType = pieceType;
        this.tile = tile;
        this.game = game;
        this.color = color;
        this.posX = this.tile.posX + parseInt(this.tile.size / 8);
        this.posY = this.tile.posY + parseInt(this.tile.size / 8);
        this.alreadyMoved = false;
    }

    /**
     * Render/draw the ChessPiece on the Tile it is placed.
     * 
     * @returns {void}
     */

    render(){
        const PATH = `images/${this.color}${this.pieceType}.png`;
        const ctx = this.game.ctx;
        
        let image = new Image();
        image.src = PATH;

        // IMPORTANT: Draw the ChessPiece only when the corresponding image
        // is loaded. If image.onload() is omitted it will only work some of 
        // the time (namely in the cases where image is loaded).
        image.onload = () => {
            ctx.drawImage(image, this.posX, this.posY);
        };
    }

    /**
    /**
     * Moves the this ChessPiece from the Tile it is currently standing
     * to the Tile where square param is pointing to.
     * @param {String} square - Square of the chessboard (a1 to h8).
     * @param {boolean} isOpponentMove - Boolean which indicates if 
     * it's the move of an opponent so it is only rendered and the
     * data is NOT send back to the server because the server
     * already registered this move.
     * 
     * @returns {void}
     */

    move(square, isOpponentMove) {
        if (!square) return;

        let nextTile = this.game.chessboard.board[square];
        const SOCKET = this.game.player.socket;

        // Send movedata to the server if it is not an opponent move.
        if (!isOpponentMove){
            SOCKET.emit("move", {
                "from": this.tile.square,
                "to": nextTile.square
            });
        }
        
        // delete nextTile from currentlyHighlighted
        // to prevent nextTile.chessPiece from being rendered twice
        // because unhighlightTiles, which is usually called after this is finished,
        // calls render for all tiles in currentlyHighlighted.
        
        this.game.chessboard.currentlyHightlighted.delete(nextTile.square);

        // Render on the Client-side.
        // Set chessPiece of previous Tile to null and render it
        // to make it disappear set nextTile to this ChessPiece and render it.
        
        this.tile.chessPiece = null;
        this.tile.render();
        nextTile.placeChessPiece(this);
        nextTile.render();
        this.alreadyMoved = true;
    }

    /**
     * Gets all legal moves for the square on which this ChessPiece
     * is placed. This action is performed asynchronously by calling the server
     * and getting the moves from the chess.js API. Subsequently after getting
     * the moves from the API the method highlightTiles is called, which
     * highlights every legal move on the board.
     * 
     * @returns {void}
     */

    getMoves(){
        let currentsquare = this.tile.square;
        this.game.player.socket.emit("getMoves", currentsquare, (moveData) => { 

            this.game.chessboard.highlightTiles(this.game.translateSAN(moveData, this));
        });

    }

}


//=======================================================================
// Subclasses of ChessPiece
//=======================================================================


/**
 * @extends ChessPiece
 */
export class King extends ChessPiece{
    constructor(pieceType, tile, color, game){
        super(pieceType, tile, color, game);
    }

    move(square, isOpponentMove){
        // Check all castling possiblities.
        const isQueenSideCastlingMoveForBlack = this.color === Color.BLACK && 
                !this.alreadyMoved &&
                square == "c8";
            
        const isQueenSideCastlingMoveForWhite = ((this.color === Color.WHITE) && 
                !this.alreadyMoved &&
                square == "c1");
            
        const isKingSideCastlingMoveForBlack = (this.color === Color.BLACK && 
                !this.alreadyMoved &&
                square == "g8");
            
        const isKingSideCastlingMoveForWhite = (this.color === Color.WHITE && 
                !this.alreadyMoved &&
                square == "g1");  
        
        super.move(square, isOpponentMove);
        // If move is not a castling move we are done.
        if (!(isQueenSideCastlingMoveForBlack || 
                isQueenSideCastlingMoveForWhite ||
                isKingSideCastlingMoveForBlack ||
                isKingSideCastlingMoveForWhite)){
            return;
            }
    
        // Get the Rook which associate with the castling...
        let rooksquare,
            moveForRook;
        
        if (isQueenSideCastlingMoveForBlack){
            rooksquare = "a8";
            moveForRook = "d8";
        } else if (isQueenSideCastlingMoveForWhite){
            rooksquare = "a1";
            moveForRook = "d1";
        } else if (isKingSideCastlingMoveForBlack){
            rooksquare = "h8";
            moveForRook = "f8";
        } else if (isKingSideCastlingMoveForWhite){
            rooksquare = "h1";
            moveForRook = "f1";
        }

        // ... and execute the move for it.
        this.game.chessboard.board[rooksquare]
            .chessPiece.move(moveForRook, isOpponentMove);
        
    }
}

/**
 * @extends ChessPiece
 */
export class Queen extends ChessPiece{
    constructor(pieceType, tile, color, game){
        super(pieceType, tile, color, game);
    }
}

/**
 * @extends ChessPiece
 */
export class Bishop extends ChessPiece {
    constructor(pieceType, tile, color, game){
        super(pieceType, tile, color, game);
    }
}

/**
 * @extends ChessPiece
 */
export class Knight extends ChessPiece {
    constructor(pieceType, tile, color, game){
        super(pieceType, tile, color, game);
    }
}

/**
 * @extends ChessPiece
 */
export class Rook extends ChessPiece {
    constructor(pieceType, tile, color, game){
        super(pieceType, tile, color, game);
    }
}

/**
 * @extends ChessPiece
 */
export class Pawn extends ChessPiece {
    constructor(pieceType, tile, color, game){
        super(pieceType, tile, color, game);
    }

    move(square, isOpponentMove){
        let nextTile = this.game.chessboard.board[square];
        let enPassantCapturedTile = null;
        let offset;
        const FIRST_RANK = 1;
        const LAST_RANK = 8;
        const SOCKET = this.game.player.socket;
        let opponentColor = (this.game.player.color === Color.BLACK) ? Color.WHITE : Color.BLACK;
        let promotedPawnColor = this.color;
        
        // Check if it is an Promotion move.
        // It is an Promotion move if the rank of the square 
        // is the first or the last Rank.
        // The Pawn get automaticially promoted to a Queen.
        // If it's an opponentMove the color of the promotedPawn is of course
        // the color of the opponent.

        if (isOpponentMove) promotedPawnColor = opponentColor;
        let promotedPawn = new Queen(PieceType.QUEEN, nextTile, promotedPawnColor, this.game); 
        if (square[1] == FIRST_RANK || square[1] == LAST_RANK){
            nextTile.chessPiece = null;
            nextTile.render();
            nextTile.placeChessPiece(promotedPawn);
            this.tile.chessPiece = null;
            this.tile.render();

            if (isOpponentMove) return;
            SOCKET.emit("move", {
                "from": this.tile.square,
                "to": nextTile.square,
                "promotion" : "q"
            });

            return;
        }

        // Check if it is an en passant move.
        // If the Pawn moves diagonally (this.tile.square[0] != square[0]
        // e.g. file a != b) and the nextTile.chessPiece is nonexistent the
        // move is an en passant move.
        
        if (nextTile.chessPiece === null && 
                this.tile.square[0] != square[0]){
            
            // If the Color of the Pawn is white the captured ChessPiece is "behind"
            // the Tile it is moving to. If the Color is black it is "in front".
            
            offset = (this.color === Color.WHITE) ? -1 : 1; 
            enPassantCapturedTile = this.game.chessboard.
                board[`${square[0]}${parseInt(square[1]) + offset}`];
            
            enPassantCapturedTile.chessPiece = null;
            
            // Render the capturedTile to make the chessPiece, which is now null,
            // disappear.
            
            enPassantCapturedTile.render();
        }

        // Call the move method in of the superclass to execute the move.
        super.move(square, isOpponentMove);
    }
}