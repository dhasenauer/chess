"use strict";

//=======================================================================
//IMPORTS
//=======================================================================

import {PieceType} from "./pieceType.js";
import {Color} from "./color.js";
import Tile from "./tile.js";
import {Pawn, Queen, 
        King, Rook, 
        Bishop, Knight} from "/chess/chessPiece.js";

//=======================================================================

/**
 * The Chessboard Class holds a reference to a game object,
 * a set of currentlyHighlighted squares and a board object
 * which is a HashMap from the square to a Tile object
 */

export default class Chessboard {

    /**
     * @constructor
     * @param {Game} game - Associated Game object.
     */

    constructor(game){
        this.currentlyHightlighted = new Set();
        this.game = game;
        this.board = this.initializeChessboard();
    }

    /**
     * Initializes the board which consists of drawing 
     * the chessboard with its ChessPieces on the canvas
     * and returning a board hashmap for saving the board state.
     * 
     * @returns {Object} - HashMap from square (a1 - h8) 
     *                     to Tile object.
     */

    initializeChessboard() {
        const CHESSBOARD_ROWS = 8,
            CHESSBOARD_COLUMNS = 8,
            SECOND_RANK = 2, 
            SEVENTH_RANK = 7,
            LAST_RANK = 8,
            FIRST_RANK = 1,
            LOWER_CASE_A_CHAR_CODE = 97,
            CHESS_PIECE_POSITIONS = [PieceType.ROOK, PieceType.KNIGHT, 
                PieceType.BISHOP, PieceType.QUEEN, PieceType.KING, 
                PieceType.BISHOP, PieceType.KNIGHT, PieceType.ROOK];

        let tileColor,
            size = 100,
            board = {},
            isBrownTile = false,
            square, 
            playerColor = this.game.player.color,
            translateTilePositionHeight,
            translateTilePositionWidth, 
            chessPiece,
            currentTile;

        
        // Render the Tiles of the Chessboard.
        for (let row = 0; row < CHESSBOARD_ROWS; row++) {
            for (let column = 0; column < CHESSBOARD_COLUMNS; column++) {
                // Mirror the chessboard layout depending if the player is black 
                // or white.
                if (playerColor == Color.BLACK) {
                    translateTilePositionHeight = this.game.canvas.height - size - column * size;
                    translateTilePositionWidth = this.game.canvas.width - size - row * size;
                } else {
                    translateTilePositionHeight = column * size;
                    translateTilePositionWidth = row * size;
                }   
                
                tileColor = isBrownTile ? "burlywood" : "wheat";
                square = String.fromCharCode(LOWER_CASE_A_CHAR_CODE + column) + (CHESSBOARD_ROWS - row);
                board[square] = new Tile(translateTilePositionHeight, translateTilePositionWidth, size, tileColor, square, this.game);
                board[square].render();
                isBrownTile = !isBrownTile;
            }
            isBrownTile = !isBrownTile;
        }

        // Render all Pawns from a2 to h2 and a7 to h7 also known
        // as the second and the seventh rank.
        for (let column = 0; column < CHESSBOARD_COLUMNS; column++) {
            currentTile = board[String.fromCharCode(LOWER_CASE_A_CHAR_CODE + column) + SEVENTH_RANK];
            chessPiece = new Pawn(PieceType.PAWN, currentTile, Color.BLACK, this.game);
            currentTile.placeChessPiece(chessPiece);

            currentTile = board[String.fromCharCode(LOWER_CASE_A_CHAR_CODE + column) + SECOND_RANK];
            chessPiece = new Pawn(PieceType.PAWN, currentTile, Color.WHITE, this.game);
            currentTile.placeChessPiece(chessPiece);
        }

        // Render the rest of the ChessPieces.
        for (let player = 0; player < 2; player++) {
            for (let column = 0; column < CHESSBOARD_COLUMNS; column++) {
                // White ChessPieces are placed on the first rank,
                // black ChessPieces on the last.
                square = String.fromCharCode(LOWER_CASE_A_CHAR_CODE + column) + 
                (playerColor == Color.BLACK ? LAST_RANK : FIRST_RANK);
                
                currentTile = board[square];
                switch (CHESS_PIECE_POSITIONS[column]) {
                    case PieceType.ROOK: {
                        chessPiece = new Rook(PieceType.ROOK, currentTile, playerColor, this.game);
                        break;
                    }

                    case PieceType.KNIGHT: {
                        chessPiece = new Knight(PieceType.KNIGHT, currentTile, playerColor, this.game);
                        break;
                    }

                    case PieceType.BISHOP: {
                        chessPiece = new Bishop(PieceType.BISHOP, currentTile, playerColor, this.game);
                        break;
                    }

                    case PieceType.QUEEN: {
                        chessPiece = new Queen(PieceType.QUEEN, currentTile, playerColor, this.game);
                        break;
                    }

                    case PieceType.KING: {
                        chessPiece = new King(PieceType.KING, currentTile, playerColor, this.game);
                        break;
                    }

                    default: {
                        break;
                    }
                }
                currentTile.placeChessPiece(chessPiece);

            }

            playerColor = (playerColor == Color.BLACK) ? Color.WHITE : Color.BLACK;
        }

        return board;
    }

    /**
     * Remove the highlighting from all Tiles which results
     * in them not being green anymore.
     * @returns {void}
     */

    unhighlightTiles(){
        for (let square of this.currentlyHightlighted){
            if (this.board[square]) this.board[square].render();
        }
        this.currentlyHightlighted.clear();
    }

    /**
     * Highlights all Tiles which results in them being marked
     * greenish.
     * 
     * @param {String[]} squareList - Array of squares (a1 - h8)
     * @returns {void}
     */

    highlightTiles(squareList){
        squareList.forEach((square) => 
            this.currentlyHightlighted.add(square.toLowerCase()));

        // Highlight for every square the corresponding Tile.
        for(let square of this.currentlyHightlighted){
            if (this.board[square]) this.board[square].highlight();
        }
    }
}