"use strict";

/**
 * Main script for playing chess.
 * @version 0.3 
 * 
 */

//=======================================================================
//IMPORTS
//=======================================================================

import Player from "./chess/player.js";

//=======================================================================

let canvas = document.getElementById("chess");

canvas.addEventListener("click", () => {
    PLAYER.makeMove();
});

let display = document.getElementById("display"),
    myNameplate = document.getElementById("nameplate1"),
    opponentsNameplate = document.getElementById("nameplate2"),
    socket = io();

const COLOR = document.querySelector("title").innerHTML,
    PLAYER = new Player(COLOR, canvas, socket, display);


PLAYER.socket.emit("sendRoomData", {
    "roomId" : document.getElementById("roomId").innerHTML,
    "color" : COLOR,
    "name" : myNameplate.innerHTML
});

PLAYER.socket.on("opponentMove", (moveData) => {
    let movedChessPiece = PLAYER.game.chessboard.board[moveData["from"]].chessPiece;
    if (!movedChessPiece) return;
    let isOpponentMove = true;
    movedChessPiece.move(moveData["to"], isOpponentMove);
    PLAYER.game.nextTurn();
});

// The Server will emit if the game is over.
PLAYER.socket.on("gameOver", (response) => {
    PLAYER.game.gameOver(response);
});

// Update the nameplate when somebody joins or leaves.
PLAYER.socket.on("joinedRoom", (name) => {
    opponentsNameplate.innerHTML = name;
    display.innerHTML = "White to move"
});

PLAYER.socket.on("leftRoom", (name) => {
    opponentsNameplate.innerHTML = "Enemy left";
});

//=======================================================================

