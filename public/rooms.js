"use strict";

$("tbody tr").each((index, tableRow) => {
    tableRow.addEventListener("click", function() { 
        location.href=`/joinRoom?roomId=${this.querySelector(".roomId").innerHTML}`;
    });
});
