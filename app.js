"use strict";

//=======================================================================
// Imports
//=======================================================================

const sessions = require("client-sessions");
const express = require("express");
const app = express();
const server = require("http").createServer(app);
const io = require("socket.io").listen(server);
const PORT = process.env.PORT || 8000;
const bodyParser = require("body-parser")
const path = require("path");
const { Chess } = require("chess.js");
const { v4: uuidv4 } = require("uuid");

//=======================================================================
// Middleware/ Globals
//=======================================================================


function notLoggedIn(request, response, next){
  if (!request.session.name) return next();
  // If the user is logged in redirect him to 
  // the rooms route.
  response.redirect("/rooms");
}
function checkIfAlreadyInRoom(request, response, next){
  // If the User is already in a room redirect him to the rooms route,
  // so he cannot create or join another room.
  for (let roomId in allRooms){
      if (allRooms[roomId].players[0] && 
              allRooms[roomId].players[0].name === request.session.name) {
      return response.redirect("/rooms");
      }
  }
  next();
}

function loggedIn(request, response, next){
  if (!request.session.name) return response.redirect("/");
  next();
}

app.use(express.static(__dirname + '/public'));
app.use(express.static('public'));
app.use(bodyParser.urlencoded({
  extended: false
}));

app.use(sessions({
  cookieName: "session",
  secret: "eedf5ab0-b59a-49e2-b5af-22f6bc426452"
}));

app.set("views", path.join(__dirname, "views"));
app.set("view-engine", "ejs");
console.log(`Server is running on Port ${PORT}.`);
server.listen(PORT);

const Color = {
  BLACK : "Black",
  WHITE : "White"
};

let allRooms = {},
  allUsers= {},
  currentlyLoggedIn = new Set();


//=======================================================================

/**
 * User class which holds the name, room and socket.
 */

class User {
  /**
   * @constructor
   * @param {String} name - User name
   * @param {Object} socket - Network socket which is associated
   * to the user.
   * @param {Color} color - Color of the User
   */
  constructor(name, socket, color){
      this.name = name;
      this.room = null;
      this.socket = socket;
      this.color = color;
      allUsers[socket.id] = this;
  }
}

/**
 * 
 * Room class which coordinates the game in a specific room.
 * Meaning a user can join a room, leave a room, ask the
 * server for available moves for a specific position, make 
 * then the move on the client-side which is then executed 
 * on the server and send to the opponent. As well as ending
 * the game in case of a game over (Checkmate, somebody left).
 * 
 */

class Room {
  /**
   * @constructor
   * @param {Color} occupied - The Color which is already occupied
   * by the Player who created the room.
   * @param {Player} host - Player who created the room
   * @param {String} name - Room name
   */

  constructor(occupied, host, name) {
      this.name = name;
      this.occupied = occupied;
      this.id = uuidv4(); // Generate a random id for the room.
      this.players = [];
      this.host = host;
      this.game = new Chess();
      this.gameStarted = false;
      this.gameEnded = false;
      this.sideToMove = Color.WHITE;
      allRooms[this.id] = this;
  }

  /**
   * User can join this room, which enables him to ask for 
   * moves at a specific position if another player has already joined, 
   * make then the move, which then is in turn made on the server and 
   * send to his opponent. After every move it is checked if the game 
   * is over yet.
   * 
   * @param {User} user 
   * @returns {void}
   */

  join(user) {
      /* When two players are in the room
      the game starts automaticially and nobody can join anymore. */
      if (this.gameStarted) return;

      let socket = user.socket,
          socketId = user.socket.id;

      user.room = this;
      allUsers[socketId] = user;
      this.players.push(user);

      // join is here the method of socket.io
      socket.join(this.id);

      if (this.players.length === 2) {
          this.gameStarted = true;

          io.to(this.players[0].socket.id).emit("joinedRoom", 
              this.players[1].name);
          io.to(this.players[1].socket.id).emit("joinedRoom", 
              this.players[0].name);
      }

      socket.on("getMoves", (moveData, sendMovesToClient) => {
          let moves = this.game.moves({
              square: moveData
          });
          if (this.gameEnded || !this.gameStarted || moves.length == 0) return;

          sendMovesToClient(moves);
      });

      socket.on("move", (moveData) => {
          this.sideToMove = (this.sideToMove == Color.WHITE) ? Color.BLACK : Color.WHITE;
          
          // Make the move and send it to the other player.
          this.game.move(moveData);
          
          // Send the move to the opponent.
          socket.to(this.id).emit('opponentMove', moveData);
          // Check after every move if the game is over.
          this.gameOver();
      });
  }

  /**
   * Checks if the game is over. If that is the case 
   * send the result back to all participants.
   * 
   * @returns {void}
   */

  gameOver() {
      let response = {};

      if (this.game.in_draw() && this.game.in_threefold_repetition()) {
          response["reason"] = "Draw: Threefold repetition";
      } else if (this.game.in_stalemate()) {
          response["reason"] = "Draw: Stalemate";
      } else if (this.game.in_draw() && this.game.insufficient_material()) {
          response["reason"] = "Draw: Insuffient Material";
      } else if (this.game.in_draw() && !this.game.insufficient_material()){
          response["reason"] = "Draw: 50-move rule";
      } else if (this.game.game_over()) {
          response["reason"] = "Checkmate";
          response["winner"] = this.sideToMove == Color.BLACK ?
            Color.WHITE : Color.BLACK;
      }


      if (this.game.in_stalemate() || this.game.in_draw() || 
            this.game.game_over()) {
          io.in(this.id).emit("gameOver", response);
          this.gameEnded = true;
      }
  }

  /**
   * If a socket leaves the room and the game has not ended, 
   * give the opponent the win and notify him his enemy
   * has left. If it has ended only notify him that the enemy has left.
   * Ultimately delete the room from memory.
   * 
   * @param {String} socketId - The id of the socket which left
   * the room.
   * @returns {void}
   */

  close(socketId) {
      // If the game has not ended a player prematurely left.
      if (!this.gameEnded) {
          let loserColor = null;
          for (let player of this.players) {
              if (socketId == player.socket.id) {
                  loserColor = player.color;
              }
          }

          let winner = (Color.BLACK == loserColor) ? Color.WHITE : Color.BLACK;

          // If another player is in the same room he gets the win.
          io.in(this.id).emit("gameOver", {
              "reason": "Checkmate",
              "winner": winner
          });
      }

      io.in(this.id).emit("leftRoom");

      // The room and user objects are subsequently deleted.
      for (let user of this.players){
        if(allUsers[user.socket.id]) delete allUsers[user.socket.id];
      }

      delete allRooms[this.id];
  }
}



//=======================================================================
// Socket Communcication
//=======================================================================

io.sockets.on("connection", (socket) => {
  console.log(`SocketId: ${socket.id} has connected.`);
  
  socket.on("sendRoomData", (data) => {
    // Instantiate the user object which belongs to the socket
    // and join the room the User has chosen. 
    let room = allRooms[data["roomId"]];
    if (room) room.join(new User(data["name"], socket, data["color"]));
  });

  socket.on("disconnect", () => {
    console.log(`SocketId: ${socket.id} has disconnected.`);
    // Close the room the User was in.
    if (allUsers[socket.id] != undefined) allUsers[socket.id].room.close(socket.id);
  });

});

//=======================================================================
//Routing
//=======================================================================

app.get("/", notLoggedIn, (request, response) => {
  response.render(path.join(__dirname, "views", "start.ejs"), {msg : ""});
});

app.get("/logout", (request, response) => {
  currentlyLoggedIn.delete(request.session.name);
  request.session.destroy();
  response.redirect("/");
});

app.post("/login", notLoggedIn, (request, response) => {
  let name = request.body.name;

  // "Login Validation" name has to be 2 to 10 characters long
  // and not already in use. 
  if (name.length < 2 || name.length > 10){
    return response.render(path.join(__dirname, "views", "start.ejs"), 
      {"msg" : "Has to be 2 - 10 Characters"})
  } else if (currentlyLoggedIn.has(name)) {
    return response.render(path.join(__dirname, "views", "start.ejs"), 
      {"msg" : "Already taken"})    
  }

  currentlyLoggedIn.add(name);
  request.session.name = name;
  response.redirect("/rooms");
});

app.get("/rooms", loggedIn, (request, response) => {
  let roomData = [];

  // Get all available rooms and show them in the
  // rooms route.
  for (let roomId in allRooms){
    if (allRooms[roomId] && 
          !allRooms[roomId].gameStarted) 
      roomData.push({
        roomId : roomId, 
        name : allRooms[roomId].name,
        host : allRooms[roomId].host
      }); 
  } 

  response.render("rooms.ejs", {roomList : roomData});
});

app.get("/createRoom", checkIfAlreadyInRoom, loggedIn, (request, response) => {
  let roomName = request.query.room;
  if (!roomName) return response.redirect("/rooms");
  // The user who creates the room gets randomly a color assigned.
  let assignedColor = (Math.random() > 0.5) ? Color.BLACK : Color.WHITE,
      newRoom = new Room(assignedColor, request.session.name, roomName);

  // Render the page with the information belonging to the client so
  // he can access it.
  response.render(path.join(__dirname, "views", "index.ejs"), {
    color : assignedColor, 
    roomId : newRoom.id,
    myName : request.session.name
  });

});

app.get("/joinRoom", checkIfAlreadyInRoom, loggedIn, (request, response) => {
  let assignedRoomId = request.query.roomId;

  if (allRooms[assignedRoomId] === undefined || 
        allRooms[assignedRoomId].gameStarted) {
    return response.redirect("/rooms");
  }

  // The user who joins the room gets the color that is not already occupied.
  let assignedColor = (allRooms[assignedRoomId].occupied == Color.BLACK) ? 
    Color.WHITE : Color.BLACK;

  response.render(path.join(__dirname, "views", "index.ejs"), {
    color : assignedColor, 
    roomId : assignedRoomId,
    myName : request.session.name
  });
});

//=======================================================================
